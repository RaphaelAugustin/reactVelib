/**
 * Created by TaF on 29/11/2016.
 */
config = require('./config/config.json')

exports.get = function (){
        url = "https://api.jcdecaux.com/vls/v1/stations?contract=Paris&apiKey="+config.JCdecauxApiKey;

        return fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.error("there on error on getting data"+ error);
                throw error;
            })
}
