***ReactVelib***

To install the project:
- rename `config/config.json.dist` to `config.json`
- in `config.json` set your API key for JCDecaux api

*for Android*
- rename `Android/app/build/main/AndroidManifest.xml.dist` to `AndroidManifest.xml`
- in `AndroidManifest.xml` set your Google API key