/**
 * Created by TaF on 29/11/2016.
 */


import React, {Component} from 'react';
import {
    AppRegistry,
    Navigator,
    StatusBar,
    TouchableHighlight,
    StyleSheet,
    Text,
    View
} from 'react-native';

import StationList from './Components/StationList';
import StationDetails from './Components/StationDetails';

const routes = [
    {title: 'StationList', index: 0},
    {title: 'StationDetails', index: 1},
]

class Main extends Component {
    render() {
        return (
            <Navigator
                initialRoute={routes[0]}
                initialRouteStack={routes}
                renderScene={
                    (route, navigator) => {

                        switch (route.index){
                         case 0:
                          return (<StationList navigator={navigator} route={routes[route.index]} {...route.passProps}/>);
                         case 1:
                          return (<StationDetails navigator={navigator} route={routes[route.index]} {...route.passProps}/>);
                        }
                    }
                }
            />
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navigationBar:{
        backgroundColor: 'darkred',
    },
    navigationBarText:{
        color: 'white',
        padding: 10,
        fontSize: 15
    },
    titleText:{
        fontSize: 20,
        paddingTop:5
    }

});



AppRegistry.registerComponent('reactVelib', () => Main);