/**
 * Created by TaF on 29/11/2016.
 */

import React, { Component, PropTypes  } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ListView,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import Data from '../Data';
import MapView from 'react-native-maps'

var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;


export default class StationList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2},
    );

        this.state = {
            dataSource: ds.cloneWithRows([]),
        }

        Data.get().then(data => {
            this.setState({dataSource: ds.cloneWithRows(data)})

        })
            .catch((error) => {
                console.error(error);
                throw error;
            })

    }


    render() {
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    initialRegion={{
                    latitude: 48.866667,
                    longitude: 2.333333,
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1
                }}
                    showsUserLocation={true}
                />
                <ListView style={styles.container}
                          dataSource={this.state.dataSource}
                          enableEmptySections={true}
                          renderRow={(rowData) =>
            <TouchableOpacity onPress={()=> this.props.navigator.push({index: 1,
               passProps:{name: rowData.name,
                status: rowData.status,
                bike_stands: rowData.bike_stands,
                available_bike_stands: rowData.available_bike_stands,
                available_bikes: rowData.available_bikes }})}>
              <View style={styles.row}>
                  <Text>{rowData.name}</Text>
                  <Text>{rowData.available_bikes}/{rowData.bike_stands}</Text>
              </View>
            </TouchableOpacity>
        }


                />
            </View>
        );
    }
}





const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    row:{
        flexDirection: 'row',
        height: 100
    },
    image:{
        height: 100
    },
    title:{
        fontSize: 20
    },
    map: {
        height: 200,
        width: width,
    },
});