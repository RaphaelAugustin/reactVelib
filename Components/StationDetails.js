/**
 * Created by TaF on 29/11/2016.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,


} from 'react-native';


export default class StationDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }

    render() {

        return (
            <View>
                <TouchableOpacity onPress={()=> this.props.navigator.push({index: 0,})}>
                <Text>
                    {this.props.name}
                </Text>
                <Text>
                    Status : {this.props.status}
                </Text>
                <Text>
                    Nombre de places : {this.props.bike_stands}
                </Text>
                <Text>
                    Places libres : {this.props.available_bike_stands}
                </Text>
                <Text>
                    Velib disponible : {this.props.available_bikes}
                </Text>
                </TouchableOpacity>
            </View>
        );
    }
}